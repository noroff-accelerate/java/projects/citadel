package no.noroff.CitadelAPI.Repositories;

import no.noroff.CitadelAPI.Models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Book getById(String id);
}

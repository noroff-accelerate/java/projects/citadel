package no.noroff.CitadelAPI.Controllers;

import java.util.*;
import java.util.function.*;

import no.noroff.CitadelAPI.Models.Author;
import no.noroff.CitadelAPI.Models.CommonResponse;
import no.noroff.CitadelAPI.Repositories.AuthorRepository;
import no.noroff.CitadelAPI.Utils.Command;
import no.noroff.CitadelAPI.Utils.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
public class AuthorController {

    @Autowired //don't forget the setter
    private AuthorRepository repository;

    @GetMapping("/author")
    public ResponseEntity<CommonResponse> authorRoot(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = null;
        cr.message = "Not implemented";

        HttpStatus resp = HttpStatus.NOT_IMPLEMENTED;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/author/all")
    public ResponseEntity<CommonResponse> getAllAuthors(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.data = repository.findAll();
        cr.message = "All authors";

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/author/{id}")
    public ResponseEntity<CommonResponse> getAuthorById(HttpServletRequest request, @PathVariable("id") Integer id){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(repository.existsById(id)) {
            cr.data = repository.findById(id);
            cr.message = "Author with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Author not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/author/{id}/books")
    public ResponseEntity<CommonResponse> getBooksbyAuthor(HttpServletRequest request, @PathVariable("id") Integer id){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(repository.existsById(id)) {
            Optional<Author> authorRepo = repository.findById(id);
            Author author = authorRepo.get();
            cr.data = author.books;
            cr.message = "Books by author with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.data = null;
            cr.message = "Author not found";
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @GetMapping("/author/search/{word}")
    public ResponseEntity<CommonResponse> getAuthorSearch(HttpServletRequest request, @PathVariable String word){
        Command cmd = new Command(request);


        String searchWord = word.toUpperCase();
        //process
        CommonResponse cr = new CommonResponse();
        List<Author> allAuthors = repository.findAll();
        ArrayList<Author> results = new ArrayList<Author>();

        for(Author author : allAuthors){
            if((author.firstname != null && author.firstname.toUpperCase().contains(searchWord))
                    || (author.lastname != null && author.lastname.toUpperCase().contains(searchWord) )
                    || (author.initials != null && author.initials.toUpperCase().contains(searchWord))){

                results.add(author);
            }
        }

        cr.data = results;
        cr.message = "Results of author search for: " + word;

        HttpStatus resp = HttpStatus.OK;

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/author")
    public ResponseEntity<CommonResponse> addAuthor(HttpServletRequest request, HttpServletResponse response, @RequestBody Author author){
        Command cmd = new Command(request);

        //process
        author = repository.save(author);

        CommonResponse cr = new CommonResponse();
        cr.data = author;
        cr.message = "New author with id: " + author.id;

        HttpStatus resp = HttpStatus.CREATED;

        response.addHeader("Location", "/author/" + author.id);

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PatchMapping("/author/{id}")
    public ResponseEntity<CommonResponse> updateAuthor(HttpServletRequest request, @RequestBody Author newAuthor, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(repository.existsById(id)) {
            Optional<Author> authorRepo = repository.findById(id);
            Author author = authorRepo.get();

            if(newAuthor.firstname != null) {
                author.firstname = newAuthor.firstname;
            }
            if(newAuthor.lastname != null) {
                author.lastname = newAuthor.lastname;
            }
            if(newAuthor.initials != null) {
                author.initials = newAuthor.initials;
            }

            repository.save(author);

            cr.data = author;
            cr.message = "Updated author with id: " + author.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Author not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PutMapping("/author/{id}")
    public ResponseEntity<CommonResponse> replaceAuthor(HttpServletRequest request, @RequestBody Author newAuthor, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(repository.existsById(id)) {
            Optional<Author> authorRepo = repository.findById(id);
            Author author = authorRepo.get();

            author.firstname = newAuthor.firstname;
            author.lastname = newAuthor.lastname;
            author.initials = newAuthor.initials;

            repository.save(author);

            cr.data = author;
            cr.message = "Replaced author with id: " + author.id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Author not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @DeleteMapping("/author/{id}")
    public ResponseEntity<CommonResponse> deleteAuthor(HttpServletRequest request, @PathVariable Integer id) {
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(repository.existsById(id)) {
            repository.deleteById(id);
            cr.message = "Deleted author with id: " + id;
            resp = HttpStatus.OK;
        } else {
            cr.message = "Author not found with id: " + id;
            resp = HttpStatus.NOT_FOUND;
        }

        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

}

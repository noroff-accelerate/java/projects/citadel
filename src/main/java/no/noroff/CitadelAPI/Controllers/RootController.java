package no.noroff.CitadelAPI.Controllers;

import no.noroff.CitadelAPI.Models.CommonResponse;
import no.noroff.CitadelAPI.Utils.Command;
import no.noroff.CitadelAPI.Utils.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@RestController
public class RootController {

    @GetMapping("/")
    public ResponseEntity<CommonResponse> landing(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.message = "hello";

        //log and return
        cmd.setResult(HttpStatus.OK);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    @GetMapping("/setup")
    public ResponseEntity<CommonResponse> setup(HttpServletRequest request){
        Command cmd = new Command(request);

        //process
        CommonResponse cr = new CommonResponse();
        cr.message = "Doing stuff ...";

        //log and return
        cmd.setResult(HttpStatus.OK);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, HttpStatus.OK);
    }

    @GetMapping("/log")
    public ResponseEntity<ArrayList<Command>> log(HttpServletRequest request){
        Command cmd = new Command(request);

        //no common response

        //log and return
        cmd.setResult(HttpStatus.OK);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<ArrayList<Command>>(Logger.getInstance().getLog(), HttpStatus.OK);
    }
}

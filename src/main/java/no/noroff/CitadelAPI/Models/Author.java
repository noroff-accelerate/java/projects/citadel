package no.noroff.CitadelAPI.Models;

import com.fasterxml.jackson.annotation.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Author {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String firstname;

    @Column
    public String initials;

    @Column
    public String lastname;

    @JsonGetter("books")
    public List<String> authors() {
        return books.stream()
                .map(book -> {
                    return "/book/" + book.id;
                }).collect(Collectors.toList());
    }

    @ManyToMany(mappedBy = "authors",fetch=FetchType.LAZY)
    public Set<Book> books = new HashSet<Book>();
}

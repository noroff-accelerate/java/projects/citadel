# Citadel API - Java Edition

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?message=Online&label=web&color=success)](https://citadel-java.herokuapp.com/)
[![SNEK approved](https://badgen.net/badge/SNEK/approved/green)]()

> Citadel API example for the Noroff Accelerate Course in Java

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install



## Usage



## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

[Dewald Els (@sumodevelopment)](https://gitlab.com/sumodevelopment)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Unlicensed © 2019 Noroff Accelerate AS
